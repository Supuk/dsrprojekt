
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ŽOGOKUP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="gettemplates.co" />


	

	
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="includes/assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="includes/assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="includes/assets/css/bootstrap.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="includes/assets/css/flexslider.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="includes/assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="includes/assets/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="includes/assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="includes/assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-2">
				<div class="fh5co-logo"><a href = "index.php"> <img src="includes/assets/images/logo.png" alt="logo" height="70" width="140"></div>
				</div>               
            </div>
            <div class="col-md-3 col-xs-4 text-right hidden-xs menu-2">
					
                </div>
			<div class="col-md-6 col-xs-4 text-center menu-1">
					<ul>
						<li class="has-dropdown">
							<a href="produkti.php">vsi produkti</a>
							
						</li>
						<li><a href="about.php">O nas</a></li>
						
						
					</ul>
                </div>
                <div class="col-md-3 col-xs-4 text-right hidden-xs menu-2">
					<ul>
						
						<li class="shopping-cart"><a href="#" class="cart"><span><small>0</small><i class="icon-shopping-cart"></i></span></a></li>
					</ul>
                </div>
		</div>
	</nav>
	
	<div id="fh5co-about">
		<div class="container">
			<div class="about-content">
				<div class="row animate-box">
					<div class="col-md-6">
						<div class="desc">
							<h3>Zgodovina podjetja</h3>
							<p>Kot prijatelji že od otroštva smo se zbrali in zacementirali kot ekipa že v 5. razredu, ko smo za izdelek na projektu "Rad imam domovino"
							naredili kranjske klobase za profesorje in starše. Od takrat naprej so z nami odraščala tudi naša načela, vizija, zagnanost predvsem pa znanje 
							ter pa neizmerna želja po nudenju najboljših storitev.</p> 
							<p>kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge kupite žoge </p>
						</div>
						<div class="desc">
							<h3>Misija &amp; Vizija</h3>
							<p>Kot pravi trije mušketirji smo tudi sami vodje naših usod in prihodnosti. Žogokup spletna trgovina je le majhen del imperija ŠEFEJi™, čigar misija je
							nadaljevati tam, kjer so spodleteli vsi prešnji diktatorji. ŠEFEJi™ bo prvi imperij, ki bo dosegel še nedoseženo: OSVOJITI IN PREVZETI CELOTEN SVET.
							Tega pa ne bomo dosegli z vojno ali nasiljem kot predhodni svetovni osvajalci. Ne ne. Naša MISIJA in VIZIJA je vse to dosešti s prijateljstvom in ljubeznijo.
						</div>
					</div>
					<div class="col-md-6">
						<img class="img-responsive" src="{{asset('frontend') }}/images/ja.jpg" alt="about">
					</div>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					
					<h2>Spoznajte našo ekipo</h2>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
					<div class="fh5co-staff">
						<img src="images/person1.jpg" alt="Free HTML5 Templates by gettemplates.co">
						<h3>Simon Černak</h3>
						<strong class="role">Priden programer</strong>
											
					</div>
				</div>
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
					<div class="fh5co-staff">
						<img src="images/person2.jpg" alt="Free HTML5 Templates by gettemplates.co">
						<h3>Abdurrahman Šupuk</h3>
						<strong class="role">Priden programer</strong>
						
						
					</div>
				</div>
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
					<div class="fh5co-staff">
						<img src="images/person3.jpg" alt="Free HTML5 Templates by gettemplates.co">
						<h3>Marko Zemljarič</h3>
						<strong class="role">Priden programer</strong>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-services" class="fh5co-bg-section">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="icon-credit-card"></i>
						</span>
						<h3>Kreditna kartica</h3>
						<p>Omogočamo preprost nakup s kartico iz udobja doma.</p>
						
					</div>
				</div>
				<div class="col-md-4 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="icon-wallet"></i>
						</span>
						<h3>Prihrani denar</h3>
						<p>Prihrani denar z našimi cenami, ki so garantirano najboljše.</p>
						
					</div>
				</div>
				<div class="col-md-4 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="icon-paper-plane"></i>
						</span>
						<h3>Brezplačna poštnina</h3>
						<p>Obračuna se v košarici pri nakupih nad 100 €.</p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="fh5co-product">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<span>Aktualno</span>
					<h2>Zadnji dodani izdelki</h2>
					<p>Poglejte si nazadnje dodane žoge. Imamo zelo široko ponudbo več na strani naši produkti.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(includes/assets/images/barca.jpg);">
							<div class="inner">
								<p>
									
									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">Nike FC Barcelona Prestige</a></h3>
							<span class="price">24.99 €</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(includes/assets/images/mls.jpg);">
							<span class="sale">Sale</span>
							<div class="inner">
								<p>
									<a href="single.html" class="icon"><i class="icon-shopping-cart"></i></a>
									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">adidas MLS CCA Official nogometna žoga</a></h3>
							<span class="price">100.99 €</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(includes/assets/images/finale.jpg);">
							<div class="inner">
								<p>
									<a href="single.html" class="icon"><i class="icon-shopping-cart"></i></a>
									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">2017-18 Official Final Champions League</a></h3>
							<span class="price">200 €</span>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="fh5co-testimonial" class="fh5co-bg-section">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<span>Feedback</span>
					<h2>Srečne stranke</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="row animate-box">
						<div class="owl-carousel owl-carousel-fullwidth">
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="includes/assets/images/marija.jpg" alt="user">
									</figure>
									<span>Marija Rozman</a></span>
									<blockquote>
										<p>&ldquo;Kupila sem  žogo za sina. Zelo zadovoljna s storitvijo, sin pa je zelo hvaležen in navdušen. Kot mati samohranilka priporočam.&rdquo;</p>
									</blockquote>
								</div>
							</div>
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="includes/assets/images/robi.jpg" alt="user">
									</figure>
									<span>Robi Valček</a></span>
									<blockquote>
										<p>&ldquo;Kupil sem nike žogo za sebe in svoje kolege, ker igramo vsak vikend in sem zelo zadovoljen. Priporočam vsem.&rdquo;</p>
									</blockquote>
								</div>
							</div>
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="includes/assets/images/samir.jpg" alt="user">
									</figure>
									<span>Samir Lebek</a></span>
									<blockquote>
										<p>&ldquo;Najboljše žoge in ponudba v Sloveniji, toplo priporočam.&rdquo;</p>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<div id="fh5co-counter" class="fh5co-bg fh5co-counter" style="background-image:url(images/img_bg_5.jpg);">
		<div class="container">
			<div class="row">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-eye"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="105599" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Ogledov</span>

							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-shopping-cart"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="50456" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Uspešnih nakupov</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-shop"></i>
								</span>
								<span class="counter js-counter" data-from="0" data-to="4" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Naše poslovalnice</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-clock"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="24" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Vedno dostopni</span>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-started">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Novice</h2>
					<p>Lahko se prijavite na naše novice. Ob vsaki novosti dobite obvestilo.</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<form class="form-inline">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="email" class="sr-only">Email</label>
								<input type="email" class="form-control" id="email" placeholder="Email">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<button type="submit" class="btn btn-default btn-block">Potrdi prijavo</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row row-pb-md">
			
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2020 ŽOGOBRC d.o.o. All Rights Reserved.</small> 
						
					</p>
					
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="includes/assets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="includes/assets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="includes/assets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="includes/assets/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="includes/assets/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="includes/assets/js/jquery.countTo.js"></script>
	<!-- Flexslider -->
	<script src="includes/assets/js/jquery.flexslider-min.js"></script>
	<!-- Main -->
	<script src="includes/assets/js/main.js"></script>

	</body>
</html>

