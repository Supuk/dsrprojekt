<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Shop &mdash; Free Website Template, Free HTML5 Template by gettemplates.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="gettemplates.co" />


	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{asset('frontend') }}/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{asset('frontend') }}/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{asset('frontend') }}/css/bootstrap.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="{{asset('frontend') }}/css/flexslider.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="{{asset('frontend') }}/css/owl.carousel.min.css">
	<link rel="stylesheet" href="{{asset('frontend') }}/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{asset('frontend') }}/css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-2">
					<div class="fh5co-logo"><a href = "index"> <img src="{{asset('frontend') }}/images/logo.png" alt="logo" height="70" width="140"></div>
				</div>               
            </div>
            <div class="col-md-3 col-xs-4 text-right hidden-xs menu-2">
					
                </div>
			<div class="col-md-6 col-xs-4 text-center menu-1">
					<ul>
						<li class="has-dropdown">
							<a href="product">vsi produkti</a>
							
						</li>
						<li><a href="about">O nas</a></li>
						
						<li><a href="contact">Kontakt</a></li>
					</ul>
                </div>
				<div class="col-md-3 col-xs-4 text-right hidden-xs menu-2">
					<ul>
						<li class="search">
							<div class="input-group">
						      <input type="text" placeholder="Search..">
						      <span class="input-group-btn">
						        <button class="btn btn-primary" type="button"><i class="icon-search"></i></button>
						      </span>
						    </div>
						</li>
						<li class="shopping-cart"><a href="#" class="cart"><span><small>0</small><i class="icon-shopping-cart"></i></span></a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/img_bg_2.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>UREDI PROFIL</h1>
							<h2><p style="background-image: url({{asset('frontend') }}/images/ekipca.jpeg);"></p></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div id="fh5co-about">
		<div class="container">
			<div class="about-content">
				<div class="row animate-box">
					<div class="col-md-6">
						<div class="desc">
							<h3>Vnesi podatke o izdelku</h3>
							Ime: <input type="text" name="ime" value="To iz baze mormo vun dat kaj ma"><br>
                            Priimek: <input type="text" name="priimek" value="To iz baze mormo vun dat kaj ma"><br>
                            Geslo: <input type="text" name="geslo" value="To iz baze mormo vun dat kaj ma"><br>
                            Novo Geslo: <input type="text" name="geslo" value="V kolikor želite novo geslo ga tukaj vnesite"><br>
						</div>
						
					</div>
					<div class="col-md-6">
                    <div class="izdelki">
							<h3>Ni vse v fizičnem izgledu dečki in deklice</h3>
							

						</div>
						
					</div>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					
					<h2>Spoznajte našo ekipo</h2>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
					<div class="fh5co-staff">
						<img src="images/person1.jpg" alt="Free HTML5 Templates by gettemplates.co">
						<h3>Simon Černak</h3>
						<strong class="role">Priden programer</strong>
						<p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
					
					</div>
				</div>
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
					<div class="fh5co-staff">
						<img src="images/person2.jpg" alt="Free HTML5 Templates by gettemplates.co">
						<h3>Abdurrahman Šupuk</h3>
						<strong class="role">Priden programer</strong>
						<p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
						
					</div>
				</div>
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
					<div class="fh5co-staff">
						<img src="images/person3.jpg" alt="Free HTML5 Templates by gettemplates.co">
						<h3>Marko Zemljarič</h3>
						<strong class="role">Priden programer</strong>
						<p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-started">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Newsletter</h2>
					<p>Just stay tune for our latest Product. Now you can subscribe</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<form class="form-inline">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="email" class="sr-only">Email</label>
								<input type="email" class="form-control" id="email" placeholder="Email">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<button type="submit" class="btn btn-default btn-block">Subscribe</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-4 fh5co-widget">
					<h3>Shop.</h3>
					<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
					<ul class="fh5co-footer-links">
						<li><a href="#">About</a></li>
						<li><a href="#">Help</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">Terms</a></li>
						<li><a href="#">Meetups</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
					<ul class="fh5co-footer-links">
						<li><a href="#">Shop</a></li>
						<li><a href="#">Privacy</a></li>
						<li><a href="#">Testimonials</a></li>
						<li><a href="#">Handbook</a></li>
						<li><a href="#">Held Desk</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
					<ul class="fh5co-footer-links">
						<li><a href="#">Find Designers</a></li>
						<li><a href="#">Find Developers</a></li>
						<li><a href="#">Teams</a></li>
						<li><a href="#">Advertise</a></li>
						<li><a href="#">API</a></li>
					</ul>
				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2020 ŽOGOBRC d.o.o. All Rights Reserved.</small> 
						
					</p>
					
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="{{asset('frontend') }}/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('frontend') }}/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="{{asset('frontend') }}/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="{{asset('frontend') }}/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="{{asset('frontend') }}/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="{{asset('frontend') }}/js/jquery.countTo.js"></script>
	<!-- Flexslider -->
	<script src="{{asset('frontend') }}/js/jquery.flexslider-min.js"></script>
	
	<!-- Main -->
	<script src="{{asset('frontend') }}/js/main.js"></script>

	</body>
</html>

 