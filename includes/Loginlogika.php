<?php
session_start();
include './dbh.php';
if (isset($_POST['submit'])) {
    $username = $_POST["username"];
    $geslo = $_POST["geslo"];
    if (empty($username) || empty($geslo)) {
        header("Location: ../login.php");
        
     exit();
    } else {
        $sql = "SELECT * FROM users WHERE username='$username';";
        $reslut = mysqli_query($conn, $sql);

        if ($row = mysqli_fetch_assoc($reslut)) {
            $hashedCheck = password_verify($geslo, $row['geslo']);
            if (!$hashedCheck) {
                header("Location: ../login.php");
               
                exit();
            } elseif ($hashedCheck) {
                $_SESSION['username'] = $row['username'];
                $_SESSION['ime'] = $row['ime'];
                $_SESSION['priimek'] = $row['priimek'];
                $_SESSION['geslo'] = $row['geslo'];
                header("Location: ../bravo.php");
                exit();
            }
        }
        else{ header("Location: ../login.php");}
    }
} else {
    header("Location: ../login.php");
    
    exit();
  
}
